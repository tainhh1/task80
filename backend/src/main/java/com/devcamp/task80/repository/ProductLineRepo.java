package com.devcamp.task80.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task80.model.ProductLine;

public interface ProductLineRepo extends JpaRepository<ProductLine, Integer> {

}
