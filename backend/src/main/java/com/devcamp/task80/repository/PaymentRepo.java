package com.devcamp.task80.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.task80.getrepository.GetFullNamePayment;
import com.devcamp.task80.model.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer> {
	List<Payment> findByCustomerId(Integer id);
	
	@Query(value = "select p.id  , concat(c.first_name, ' ' , c.last_name) fullName\r\n"
			+ ", p.check_number checkNumber , p.payment_date paymentDate , p.ammount \r\n"
			+ "from payments p \r\n"
			+ "join customers c on p.customer_id = c.id ", nativeQuery = true)
	List<GetFullNamePayment> getPayment();
}
