package com.devcamp.task80.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.task80.getrepository.GetProductNameOrderDetail;
import com.devcamp.task80.model.OrderDetail;

@Repository
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Integer> {
	@Query(value = "select od.id , od.order_id orderId, p.product_name productName,"
			+ " od.quantity_order quantityOrder, od.price_each priceEach\r\n"
			+ "from order_details od \r\n"
			+ "join products p on od.product_id = p.id ", nativeQuery = true)
	List<GetProductNameOrderDetail> orderDetailWithProductName();
}
