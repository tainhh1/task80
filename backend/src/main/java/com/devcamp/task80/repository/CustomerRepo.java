package com.devcamp.task80.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task80.getrepository.GetOrderAndPayment;
import com.devcamp.task80.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	@Query(value = "select o.order_date orderDate, o.required_date requiredDate,\r\n"
			+ "o.shipped_date shippedDate, o.status , o.comments,\r\n"
			+ "p.check_number checkNumber, p.payment_date paymentDate, p.ammount \r\n"
			+ "from customers c \r\n"
			+ "join orders o ON o.customer_id = c.id \r\n"
			+ "join payments p on p.customer_id  = c.id \r\n"
			+ "where c.id = :customerId", nativeQuery = true)
	List<GetOrderAndPayment> getOrderAndPaymentsByCustomerId(@Param("customerId") int customerId);
}
