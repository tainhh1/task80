package com.devcamp.task80.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task80.getrepository.GetFullNameOrderDetail;
import com.devcamp.task80.getrepository.GetOrderByFullName;
import com.devcamp.task80.model.Order;

@Repository
public interface OrderRepo extends JpaRepository<Order, Integer> {
	@Query(value = "select concat(c.first_name, ' ', c.last_name) fullName, od.id , od.order_id orderId,"
			+ " od.product_id productId, od.quantity_order quantityOrder, od.price_each priceEach \r\n"
			+ "from orders o \r\n"
			+ "join order_details od on o.id = od.order_id \r\n"
			+ "join customers c on o.customer_id = c.id \r\n"
			+ "where o.id = :orderId", nativeQuery = true)
	List<GetFullNameOrderDetail> fullNameOrderDetail(@Param("orderId") int orderId);
	
	@Query(value = "select o.id ,concat(c.first_name, ' ', c.last_name) fullName,\r\n"
			+ "o.order_date orderDate, o.required_date requiredDate, o.shipped_date shippedDate,\r\n"
			+ "o.status , o.comments \r\n"
			+ "from orders o \r\n"
			+ "join customers c on o.customer_id = c.id \r\n"
			+ "order by fullName", nativeQuery = true)
	List<GetOrderByFullName> getOrderOnSortFullName();
}
