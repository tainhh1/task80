package com.devcamp.task80;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task80Application {

	public static void main(String[] args) {
		SpringApplication.run(Task80Application.class, args);
	}

}
