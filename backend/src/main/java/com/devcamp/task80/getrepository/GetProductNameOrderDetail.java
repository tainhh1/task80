package com.devcamp.task80.getrepository;

public interface GetProductNameOrderDetail {
	public int getId();
	public int getOrderId();
	public String getProductName();
	public int getQuantityOrder();
	public java.math.BigDecimal getPriceEach();
}
