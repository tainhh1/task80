package com.devcamp.task80.getrepository;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface GetOrderAndPayment {
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getOrderDate();
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getRequiredDate();
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getShippedDate();
	public String getStatus();
	public String getComments();
	public String getCheckNumber();
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getPaymentDate();
	public java.math.BigDecimal getAmmount();
}
