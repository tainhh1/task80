package com.devcamp.task80.getrepository;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface GetOrderByFullName {
	public int getId();
	public String getFullName();
	
	@JsonFormat(pattern = "yyyy/MM/dd")
	public Date getOrderDate();
	
	@JsonFormat(pattern = "yyyy/MM/dd")
	public Date getRequiredDate();
	
	@JsonFormat(pattern = "yyyy/MM/dd")
	public Date getShippedDate();
	
	public String getStatus();
	public String getComments();
}
