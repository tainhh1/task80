package com.devcamp.task80.getrepository;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface GetFullNamePayment {
	public int getId();
	
	public String getFullName();
	
	public String getCheckNumber();
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	public Date getPaymentDate();
	
	public java.math.BigDecimal getAmmount();
}
