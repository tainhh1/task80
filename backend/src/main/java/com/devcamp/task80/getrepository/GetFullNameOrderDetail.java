package com.devcamp.task80.getrepository;

public interface GetFullNameOrderDetail {
	public String getFullName();
	public int getId();
	public int getOrderId();
	public int getProductId();
	public int getQuantityOrder();
	public java.math.BigDecimal getPriceEach();
}
