$(document).ready(() => {
    // Khai báo table
    let gEmployeeTable = $('#table-employee').DataTable({
        columns: [
            { data: 'firstName' },
            { data: 'lastName' },
            { data: 'extension' },
            { data: 'email' },
            { data: 'officeCode' },
            { data: 'reportTo' },
            { data: 'jobTitle' },
            { data: 'Action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });
    function loadEmployeeTable(paramEmployee) {
        gEmployeeTable.clear();
        gEmployeeTable.rows.add(paramEmployee);
        gEmployeeTable.draw();
    }

    let gEmployeeId = 0;
    //  object employee
    let gEmployee = {
        // getAll Employee
        employeeDb: '',
        getAllEmployee() {
            $.ajax({
                type: 'get',
                url: 'http://localhost:8080/api/employees',
                dataType: 'json',
                async: false,
                success: function (res) {
                    gEmployee.employeeDb = res;
                    loadEmployeeTable(res);
                },
                error: (e) => alert(e.responseText),
            });
        },
        checkEmail(paramEmail) {
            return this.employeeDb.some((employee) => employee.email == paramEmail);
        },
        // create & update employee
        onCreateEmployeeClick() {
            $('#modal-save').modal('show');
            gEmployeeId = 0;
            resetForm();
        },
        onUpdateEmployeeClick() {
            $('#modal-save').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gEmployeeTable.row(vSelectedRow).data();
            gEmployeeId = vSelectedData.id;
            $.getJSON(`http://localhost:8080/api/employees/${gEmployeeId}`, loadEmployeeToInput);
        },
        // save employee
        newEmployee: {
            lastName: '',
            firstName: '',
            extension: '',
            email: '',
            officeCode: '',
            reportTo: '',
            jobTitle: '',
        },
        onSaveEmployeeClick() {
            this.newEmployee = {
                lastName: $('#input-last-name').val().trim(),
                firstName: $('#input-first-name').val().trim(),
                extension: $('#input-extension').val().trim(),
                email: $('#input-email').val().trim(),
                officeCode: $('#input-office-code').val().trim(),
                reportTo: $('#input-report-to').val().trim(),
                jobTitle: $('#input-job-title').val().trim(),
            };
            if (validateEmployee(this.newEmployee)) {
                gEmployee.saveEmployee(this.newEmployee);
            }
        },
        saveEmployee(paramEmployee) {
            if (gEmployeeId == 0) {
                $.ajax({
                    type: 'post',
                    url: `http://localhost:8080/api/employees`,
                    data: JSON.stringify(paramEmployee),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`success create new employee`);
                        $('#modal-save').modal('hide');
                        gEmployee.getAllEmployee();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'put',
                    url: `http://localhost:8080/api/employees/${gEmployeeId}`,
                    data: JSON.stringify(paramEmployee),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`success update employee`);
                        $('#modal-save').modal('hide');
                        gEmployee.getAllEmployee();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // deleteEmployee
        onDeleteEmployeeClick() {
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gEmployeeTable.row(vSelectedRow).data();
            gEmployeeId = vSelectedData.id;
            $('#modal-delete').modal('show');
        },
        onDeleteAllEmployeeClick() {
            gEmployeeId = 0;
            $('#modal-delete').modal('show');
        },
        onConfirmDeleteEmployeeClick() {
            if (gEmployeeId == 0) {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/employees`,
                    success: function (response) {
                        alert('success delete employee');
                        $('#modal-delete').modal('hide');
                        gEmployee.getAllEmployee();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/employees/${gEmployeeId}`,
                    success: function (response) {
                        alert('success delete employee');
                        $('#modal-delete').modal('hide');
                        gEmployee.getAllEmployee();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
    };
    gEmployee.getAllEmployee();

    // adde event listener
    $('#btn-create-employee').click(gEmployee.onCreateEmployeeClick);
    $('#table-employee').on('click', '.fa-edit', gEmployee.onUpdateEmployeeClick);
    $('#btn-save-info').click(gEmployee.onSaveEmployeeClick);
    $('#table-employee').on('click', '.fa-trash-alt', gEmployee.onDeleteEmployeeClick);
    $('#btn-delete-all-employee').click(gEmployee.onDeleteAllEmployeeClick);
    $('#btn-confirm-delete').click(gEmployee.onConfirmDeleteEmployeeClick);

    // validate employee
    function validateEmployee(paramEmployee) {
        let vResult = true;
        try {
            if (paramEmployee.firstName == '') {
                vResult = false;
                throw `first name can't be empty`;
            }
            if (paramEmployee.lastName == '') {
                vResult = false;
                throw `last name can't be empty`;
            }
            if (paramEmployee.extension == '') {
                vResult = false;
                throw `extension can't be empty`;
            }
            if (paramEmployee.email == '') {
                vResult = false;
                throw `email can't be empty`;
            }
            if (gEmployeeId == 0) {
                if (gEmployee.checkEmail(paramEmployee.email)) {
                    vResult = false;
                    throw `email is Existed`;
                }
            }
            if (!validateEmail(paramEmployee.email)) {
                vResult = false;
                throw `need a right email`;
            }
            if (paramEmployee.officeCode == '') {
                vResult = false;
                throw `office code can't be empty`;
            }
            if (paramEmployee.reportTo == '') {
                vResult = false;
                throw `report to can't be empty`;
            }
            if (paramEmployee.jobTitle == '') {
                vResult = false;
                throw `job title can't be empty`;
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // validate email
    function validateEmail(email) {
        const re =
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    // load employee to input
    function loadEmployeeToInput(paramEmployee) {
        $('#input-first-name').val(paramEmployee.firstName);
        $('#input-last-name').val(paramEmployee.lastName);
        $('#input-extension').val(paramEmployee.extension);
        $('#input-email').val(paramEmployee.email);
        $('#input-office-code').val(paramEmployee.officeCode);
        $('#input-report-to').val(paramEmployee.reportTo);
        $('#input-job-title').val(paramEmployee.jobTitle);
    }

    // reset form
    function resetForm() {
        $('#input-first-name').val('');
        $('#input-last-name').val('');
        $('#input-extension').val('');
        $('#input-email').val('');
        $('#input-office-code').val('');
        $('#input-report-to').val('');
        $('#input-job-title').val('');
    }
});
