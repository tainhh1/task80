$(document).ready(() => {
    // khai bao table
    let gOfficeTable = $('#table-office').DataTable({
        columns: [
            { data: 'city' },
            { data: 'phone' },
            { data: 'addressLine' },
            { data: 'state' },
            { data: 'country' },
            { data: 'territory' },
            { data: 'Action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });
    function loadOfficeTable(paramOffice) {
        gOfficeTable.clear();
        gOfficeTable.rows.add(paramOffice);
        gOfficeTable.draw();
    }

    let gOfficeId = 0;
    // object office
    let gOffice = {
        // get all office
        getAllOffice() {
            $.getJSON('http://localhost:8080/api/offices', loadOfficeTable);
        },
        // create & update
        onCreateOfficeClick() {
            $('#modal-save').modal('show');
            gOfficeId = 0;
            resetForm();
        },
        onUpdateOfficeClick() {
            $('#modal-save').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOfficeTable.row(vSelectedRow).data();
            gOfficeId = vSelectedData.id;
            $.get(`http://localhost:8080/api/offices/${gOfficeId}`, loadOfficeToInput);
        },
        // save office
        newOffice: {
            city: '',
            phone: '',
            addressLine: '',
            state: '',
            country: '',
            territory: '',
        },
        onSaveOfficeClick() {
            this.newOffice = {
                city: $('#input-city').val().trim(),
                phone: $('#input-phone').val().trim(),
                addressLine: $('#input-address').val().trim(),
                state: $('#input-state').val().trim(),
                country: $('#input-country').val().trim(),
                territory: $('#input-territory').val().trim(),
            };
            if (validateOffice(this.newOffice)) {
                gOffice.saveOffice(this.newOffice);
            }
        },
        saveOffice(paramOffice) {
            if (gOfficeId == 0) {
                $.ajax({
                    type: 'post',
                    url: `http://localhost:8080/api/offices`,
                    data: JSON.stringify(paramOffice),
                    contentType: 'application/json',
                    success: function (response) {
                        alert('successfully create new office');
                        $('#modal-save').modal('hide');
                        gOffice.getAllOffice();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'put',
                    url: `http://localhost:8080/api/offices/${gOfficeId}`,
                    data: JSON.stringify(paramOffice),
                    contentType: 'application/json',
                    success: function (response) {
                        alert('successfully update office');
                        $('#modal-save').modal('hide');
                        gOffice.getAllOffice();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // delete office
        onDeleteAllOfficeClick() {
            $('#modal-delete').modal('show');
            gOfficeId = 0;
        },
        onDeleteOfficeClick() {
            $('#modal-delete').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOfficeTable.row(vSelectedRow).data();
            gOfficeId = vSelectedData.id;
        },
        onConfirmDeleteOfficeClick() {
            if (gOfficeId == 0) {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/offices`,
                    success: function (response) {
                        $('#modal-delete').modal('hide');
                        alert('successfully delete office');
                        gOffice.getAllOffice();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/offices/${gOfficeId}`,
                    success: function (response) {
                        $('#modal-delete').modal('hide');
                        alert('successfully delete office');
                        gOffice.getAllOffice();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
    };
    gOffice.getAllOffice();

    // add event listener
    $('#btn-create-office').click(gOffice.onCreateOfficeClick);
    $('#table-office').on('click', '.fa-edit', gOffice.onUpdateOfficeClick);
    $('#btn-save-info').click(gOffice.onSaveOfficeClick);
    $('#btn-delete-all-office').click(gOffice.onDeleteAllOfficeClick);
    $('#table-office').on('click', '.fa-trash-alt', gOffice.onDeleteOfficeClick);
    $('#btn-confirm-delete').click(gOffice.onConfirmDeleteOfficeClick);
    // validate office
    function validateOffice(paramOffice) {
        let vResult = true;
        try {
            if (paramOffice.city == '') {
                vResult = false;
                throw `city can't be empty`;
            }
            if (paramOffice.phone == '') {
                vResult = false;
                throw `phone can't be empty`;
            }
            if (paramOffice.addressLine == '') {
                vResult = false;
                throw `address Line can't be empty`;
            }
            if (paramOffice.country == '') {
                vResult = false;
                throw `country can't be empty`;
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // load office to input
    function loadOfficeToInput(paramOffice) {
        $('#input-city').val(paramOffice.city);
        $('#input-phone').val(paramOffice.phone);
        $('#input-address').val(paramOffice.addressLine);
        $('#input-state').val(paramOffice.state);
        $('#input-country').val(paramOffice.country);
        $('#input-territory').val(paramOffice.territory);
    }

    // reset form
    function resetForm() {
        $('#input-city').val('');
        $('#input-phone').val('');
        $('#input-address').val('');
        $('#input-state').val('');
        $('#input-country').val('');
        $('#input-territory').val('');
    }
});
