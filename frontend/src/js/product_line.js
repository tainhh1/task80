$(document).ready(() => {
    // khoi tao data table
    let gProductLineTable = $('#table-product-line').DataTable({
        columns: [{ data: 'productLine' }, { data: 'description' }, { data: 'Action' }],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });

    function loadProductLineToTable(paramProductLine) {
        gProductLineTable.clear();
        gProductLineTable.rows.add(paramProductLine);
        gProductLineTable.draw();
    }

    let gProductLineId = 0;
    // object product line
    let gProductLine = {
        // get product Line
        productLineDb: '',
        getAllProductLine() {
            $.ajax({
                type: 'get',
                url: 'http://localhost:8080/api/product-lines',
                dataType: 'json',
                async: false,
                success: function (res) {
                    gProductLine.productLineDb = res;
                    loadProductLineToTable(res);
                },
                error: (e) => alert(e.responseText),
            });
        },
        checkProductLine(paramProductLine) {
            return this.productLineDb.some((product) => product.productLine == paramProductLine);
        },
        // create & update product line
        onCreateProductLineClick() {
            gProductLineId = 0;
            resetForm();
            $('#modal-save').modal('show');
        },
        onUpdateProductLineClick() {
            $('#modal-save').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gProductLineTable.row(vSelectedRow).data();
            gProductLineId = vSelectedData.id;
            $.get(
                `http://localhost:8080/api/product-lines/${gProductLineId}`,
                loadProductLineToInput,
            );
        },
        // save product line
        newProductLine: {
            productLine: '',
            description: '',
        },
        onSaveProductLineClick() {
            this.newProductLine = {
                productLine: $('#input-product-line').val().trim(),
                description: $('#input-description').val().trim(),
            };
            if (validateProductLine(this.newProductLine)) {
                gProductLine.saveProductLine(this.newProductLine);
            }
        },
        saveProductLine(paramProductLine) {
            if (gProductLineId == 0) {
                $.ajax({
                    type: 'post',
                    url: `http://localhost:8080/api/product-lines`,
                    data: JSON.stringify(paramProductLine),
                    contentType: 'application/json',
                    success: function (response) {
                        alert('successfully create new product line');
                        $('#modal-save').modal('hide');
                        gProductLine.getAllProductLine();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'put',
                    url: `http://localhost:8080/api/product-lines/${gProductLineId}`,
                    data: JSON.stringify(paramProductLine),
                    contentType: 'application/json',
                    success: function (response) {
                        alert('successfully update product line');
                        $('#modal-save').modal('hide');
                        gProductLine.getAllProductLine();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // delete product line
        onDeleteProductLineClick() {
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gProductLineTable.row(vSelectedRow).data();
            gProductLineId = vSelectedData.id;
            $('#modal-delete').modal('show');
        },
        onDeleteAllProductLineClick() {
            $('#modal-delete').modal('show');
            gProductLineId = 0;
        },
        onConfirmDeleteProductLineClick() {
            if (gProductLineId == 0) {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/product-lines`,
                    success: function (response) {
                        $('#modal-delete').modal('hide');
                        alert('successfully delete product line');
                        gProductLine.getAllProductLine();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/product-lines/${gProductLineId}`,
                    success: function (response) {
                        $('#modal-delete').modal('hide');
                        alert('successfully delete product line');
                        gProductLine.getAllProductLine();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
    };

    gProductLine.getAllProductLine();

    // add event listener
    $('#btn-create-product-line').click(gProductLine.onCreateProductLineClick);
    $('#table-product-line').on('click', '.fa-edit', gProductLine.onUpdateProductLineClick);
    $('#btn-save-info').click(gProductLine.onSaveProductLineClick);
    $('#table-product-line').on('click', '.fa-trash-alt', gProductLine.onDeleteProductLineClick);
    $('#btn-delete-all-product-line').click(gProductLine.onDeleteAllProductLineClick);
    $('#btn-confirm-delete').click(gProductLine.onConfirmDeleteProductLineClick);

    function validateProductLine(paramProductLine) {
        let vResult = true;
        try {
            if (paramProductLine.productLine == '') {
                vResult = false;
                throw `100. product line can't be empty`;
            }
            if (gProductLineId == 0) {
                if (gProductLine.checkProductLine(paramProductLine.description)) {
                    vResult = false;
                    throw `110. product line is Existed`;
                }
            }
            if (paramProductLine.description == '') {
                vResult = false;
                throw `102. product description can't be empty`;
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // load product line to input
    function loadProductLineToInput(paramProductLine) {
        $('#input-product-line').val(paramProductLine.productLine);
        $('#input-description').val(paramProductLine.description);
    }

    // reset form
    function resetForm() {
        $('#input-product-line').val('');
        $('#input-description').val('');
    }
});
