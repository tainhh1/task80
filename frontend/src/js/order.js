$(document).ready(() => {
    // get customer cho payment
    $.get(`http://localhost:8080/api/customers`, getAllCustomer);
    function getAllCustomer(paramCustomer) {
        let selectElement = $('#select-customer').select2({
            theme: 'bootstrap4',
        });
        paramCustomer.forEach((customer) => {
            $(`<option>`, {
                text: `${customer.firstName} ${customer.lastName}`,
                value: customer.id,
            }).appendTo(selectElement);
        });
    }

    // khai bao dataTable
    let gOrderTable = $('#table-order').DataTable({
        columns: [
            { data: 'fullName' },
            { data: 'orderDate' },
            { data: 'requiredDate' },
            { data: 'shippedDate' },
            { data: 'status' },
            { data: 'comments' },
            { data: 'Action' },
            { data: 'id' },
        ],
        columnDefs: [
            {
                targets: -2,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
            {
                targets: -1,
                render: (id) =>
                    `<button class="btn btn-primary btn-get-detail" data-id="${id}">Get Order Detail</button>`,
            },
        ],
    });

    // load Order to table
    function loadOrderToTable(paramOrder) {
        gOrderTable.clear();
        gOrderTable.rows.add(paramOrder);
        gOrderTable.draw();
    }

    // khai bao order detail table
    let gOrderDetailTable = $('#table-order-detail').DataTable({
        columns: [
            { data: 'id' },
            { data: 'fullName' },
            { data: 'productId' },
            { data: 'orderId' },
            { data: 'quantityOrder' },
            { data: 'priceEach' },
        ],
    });
    function loadOrderDetailToTable(paramOrderDetail) {
        $('#modal-order-detail').modal('show');
        gOrderDetailTable.clear();
        gOrderDetailTable.rows.add(paramOrderDetail);
        gOrderDetailTable.draw();
    }

    // add event change
    $('#select-customer').change((e) => (gCustomerId = e.target.value));

    // object order
    let gCustomerId = 0;
    let gOrderId = 0;
    let gOrder = {
        getAllOrder() {
            $.get('http://localhost:8080/api/orders/fullname', loadOrderToTable, 'json');
        },
        // create & update order
        onCreateOrderClick() {
            gOrderId = 0;
            $('#select-customer').prop('disabled', false);
            resetForm();
            $('#modal-save').modal('show');
        },
        onUpdateOrderClick() {
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOrderTable.row(vSelectedRow).data();
            gOrderId = vSelectedData.id;
            $('#modal-save').modal('show');
            $('#select-customer').prop('disabled', true);
            $.getJSON(`http://localhost:8080/api/orders/${gOrderId}`, loadOrderToInput);
        },
        // save info
        newOrder: {
            orderDate: '',
            requiredDate: '',
            shippedDate: '',
            status: '',
            comments: '',
        },
        onSaveInfoClick() {
            this.newOrder = {
                orderDate: $('#input-order-date').val().trim(),
                requiredDate: $('#input-required-date').val().trim(),
                shippedDate: $('#input-shipped-date').val().trim(),
                status: $('#input-order-status').val().trim(),
                comments: $('#input-order-comments').val().trim(),
            };
            if (validateOrder(this.newOrder)) {
                gOrder.saveOrder(this.newOrder);
            }
        },
        saveOrder(paramOrder) {
            if (gOrderId == 0) {
                $.ajax({
                    type: 'post',
                    url: `http://localhost:8080/api/customers/${gCustomerId}/orders`,
                    data: JSON.stringify(paramOrder),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`successfully create new order`);
                        $('#modal-save').modal('hide');
                        gOrder.getAllOrder();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'put',
                    url: `http://localhost:8080/api/orders/${gOrderId}`,
                    data: JSON.stringify(paramOrder),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`successfully update order`);
                        $('#modal-save').modal('hide');
                        gOrder.getAllOrder();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // delete order
        onDeleteAllOrderClick() {
            $('#modal-delete').modal('show');
            gOrderId = 0;
        },
        onDeleteOrderClick() {
            $('#modal-delete').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOrderTable.row(vSelectedRow).data();
            gOrderId = vSelectedData.id;
        },
        onConfirmDeleteOrderClick() {
            if (gOrderId == 0) {
                $.ajax({
                    type: 'delete',
                    url: 'http://localhost:8080/api/orders',
                    success: function (response) {
                        alert(`successfully delete all Order`);
                        $('#modal-delete').modal('hide');
                        gOrder.getAllOrder();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/orders/${gOrderId}`,
                    success: function (response) {
                        alert(`successfully delete Order`);
                        $('#modal-delete').modal('hide');
                        gOrder.getAllOrder();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // orderDetail
        onGetOrderDetailClick(e) {
            let vOrderId = $(e.target).data('id');
            $.get(
                `http://localhost:8080/api/fullnames/orders/${vOrderId}`,
                loadOrderDetailToTable,
                'json',
            );
        },
    };
    gOrder.getAllOrder();

    // add event listener
    $('#btn-create-order').click(gOrder.onCreateOrderClick);
    $('#table-order').on('click', '.fa-edit', gOrder.onUpdateOrderClick);
    $('#btn-save-info').click(gOrder.onSaveInfoClick);
    $('#btn-delete-all-order').click(gOrder.onDeleteAllOrderClick);
    $('#table-order').on('click', '.fa-trash-alt', gOrder.onDeleteOrderClick);
    $('#btn-confirm-delete').click(gOrder.onConfirmDeleteOrderClick);
    $('#table-order').on('click', '.btn-get-detail', gOrder.onGetOrderDetailClick);
    // validate Order
    function validateOrder(paramOrder) {
        let vResult = true;
        try {
            if (paramOrder.orderDate == '') {
                vResult = false;
                throw `100.Order date can't be empty`;
            }
            if (paramOrder.requiredDate == '') {
                vResult = false;
                throw `200.Required date can't be empty`;
            }
            if (gOrderId == 0) {
                if (gCustomerId == 0) {
                    vResult = false;
                    throw `300. Please select customer to create order`;
                }
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // load Order to Input
    function loadOrderToInput(paramOrder) {
        $('#input-order-date').val(paramOrder.orderDate);
        $('#input-required-date').val(paramOrder.requiredDate);
        $('#input-shipped-date').val(paramOrder.shippedDate);
        $('#input-order-status').val(paramOrder.status);
        $('#input-order-comments').val(paramOrder.comments);
    }

    //reset form
    function resetForm() {
        $('#select-customer').val(0);
        $('#input-order-date').val('');
        $('#input-required-date').val('');
        $('#input-shipped-date').val('');
        $('#input-order-status').val('');
        $('#input-order-comments').val('');
    }
});
