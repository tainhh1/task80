$(document).ready(() => {
    // load product line to select
    $.getJSON('http://localhost:8080/api/product-lines', loadProductLineToSelect);
    function loadProductLineToSelect(paramProductLine) {
        let vSelectElement = $('#select-product-line').select2({ theme: 'bootstrap4' });
        paramProductLine.forEach((productLine) => {
            $('<option>', {
                text: productLine.productLine,
                value: productLine.id,
            }).appendTo(vSelectElement);
        });
    }

    // khai bao table
    let gProductTable = $('#table-product').DataTable({
        columns: [
            { data: 'productCode' },
            { data: 'productName' },
            { data: 'productDescription' },
            { data: 'productScale' },
            { data: 'productVendor' },
            { data: 'quantityInStock' },
            { data: 'buyPrice' },
            { data: 'Action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });

    function loadProductToTable(paramProduct) {
        gProductTable.clear();
        gProductTable.rows.add(paramProduct);
        gProductTable.draw();
    }

    // add change event listener
    $('#select-product-line').change((e) => (gProductLineId = e.target.value));

    let gProductId = 0;
    let gProductLineId = 0;
    // object product
    let gProduct = {
        // get All product
        productDb: '',
        getAllProduct() {
            $.ajax({
                type: 'get',
                url: 'http://localhost:8080/api/products',
                dataType: 'json',
                async: false,
                success: function (products) {
                    gProduct.productDb = products;
                    loadProductToTable(products);
                },
            });
        },
        //
        checkProductCode(paramProductCode) {
            return this.productDb.some((product) => product.productCode == paramProductCode);
        },
        // create & update Product
        onCreateProductClick() {
            $('#modal-save').modal('show');
            gProductId = 0;
            $('#select-product-line').prop('disabled', false);
            resetForm();
        },
        onUpdateProductClick() {
            $('#modal-save').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gProductTable.row(vSelectedRow).data();
            gProductId = vSelectedData.id;
            $('#select-product-line').prop('disabled', true);
            $.getJSON(`http://localhost:8080/api/products/${gProductId}`, loadProductToInput);
        },
        // save product
        newProduct: {
            productCode: '',
            productName: '',
            productDescription: '',
            productScale: '',
            productVendor: '',
            quantityInStock: '',
            buyPrice: '',
        },
        onSaveProductClick() {
            this.newProduct = {
                productCode: $('#input-product-code').val().trim(),
                productName: $('#input-product-name').val().trim(),
                productDescription: $('#input-product-description').val().trim(),
                productScale: $('#input-product-scale').val().trim(),
                productVendor: $('#input-product-vendor').val().trim(),
                quantityInStock: $('#input-product-quantity').val().trim(),
                buyPrice: $('#input-product-price').val().trim(),
            };
            if (validateProduct(this.newProduct)) {
                gProduct.saveProduct(this.newProduct);
            }
        },
        saveProduct(paramProduct) {
            if (gProductId == 0 && gProductLineId == 0) {
                saveProductHaveProductLine(paramProduct);
            } else if (gProductId == 0) {
                saveProductNoProductLine(paramProduct);
            } else {
                updateProduct(paramProduct);
            }
        },
        // delete Product
        onDeleteProductClick() {
            $('#modal-delete').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gProductTable.row(vSelectedRow).data();
            gProductId = vSelectedData.id;
        },
        onDeleteAllProductClick() {
            $('#modal-delete').modal('show');
            gProductId = 0;
        },
        onConfirmDeleteClick() {
            if (gProductId == 0) {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/products`,
                    success: function (response) {
                        alert(`successfully delete all products`);
                        gProduct.getAllProduct();
                        $('#modal-delete').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/products/${gProductId}`,
                    success: function (response) {
                        alert(`successfully delete products`);
                        gProduct.getAllProduct();
                        $('#modal-delete').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
    };
    gProduct.getAllProduct();

    // add event listener
    $('#btn-create-product').click(gProduct.onCreateProductClick);
    $('#table-product').on('click', '.fa-edit', gProduct.onUpdateProductClick);
    $('#btn-save-info').click(gProduct.onSaveProductClick);
    $('#table-product').on('click', '.fa-trash-alt', gProduct.onDeleteProductClick);
    $('#btn-delete-all-product').click(gProduct.onDeleteAllProductClick);
    $('#btn-confirm-delete').click(gProduct.onConfirmDeleteClick);
    // function save product
    function saveProductHaveProductLine(paramProduct) {
        $.ajax({
            type: 'post',
            url: `http://localhost:8080/api/products?productLineId=${gProductLineId}`,
            data: JSON.stringify(paramProduct),
            contentType: 'application/json',
            success: function (response) {
                alert(`successfully create new product`);
                gProduct.getAllProduct();
                $('#modal-save').modal('hide');
            },
            error: (e) => alert(e.responseText),
        });
    }

    function saveProductNoProductLine(paramProduct) {
        $.ajax({
            type: 'post',
            url: `http://localhost:8080/api/products`,
            data: JSON.stringify(paramProduct),
            contentType: 'application/json',
            success: function (response) {
                alert(`successfully create new product`);
                gProduct.getAllProduct();
                $('#modal-save').modal('hide');
            },
            error: (e) => alert(e.responseText),
        });
    }

    function updateProduct(paramProduct) {
        $.ajax({
            type: 'put',
            url: `http://localhost:8080/api/products/${gProductId}`,
            data: JSON.stringify(paramProduct),
            contentType: 'application/json',
            success: function (response) {
                alert(`successfully update product`);
                gProduct.getAllProduct();
                $('#modal-save').modal('hide');
            },
            error: (e) => alert(e.responseText),
        });
    }

    // validate product
    function validateProduct(paramProduct) {
        let vResult = true;
        try {
            if (paramProduct.productCode == '') {
                vResult = false;
                throw `100.product code can't be empty`;
            }
            if (gProductId == 0) {
                if (gProduct.checkProductCode(paramProduct.productCode)) {
                    vResult = false;
                    throw `101. product code is Existed`;
                }
            }
            if (paramProduct.productName == '') {
                vResult = false;
                throw `200.product name can't be empty`;
            }
            if (paramProduct.productScale == '') {
                vResult = false;
                throw `300.product scale can't be empty`;
            }
            if (paramProduct.productVendor == '') {
                vResult = false;
                throw `400.product vendor can't be empty`;
            }
            if (paramProduct.quantityInStock == '') {
                vResult = false;
                throw `500.Quantity can't be empty`;
            }
            if (paramProduct.buyPrice == '') {
                vResult = false;
                throw `600.Price can't be empty`;
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // load product to input
    function loadProductToInput(paramProduct) {
        $('#input-product-code').val(paramProduct.productCode);
        $('#input-product-name').val(paramProduct.productName);
        $('#input-product-description').val(paramProduct.productDescription);
        $('#input-product-scale').val(paramProduct.productScale);
        $('#input-product-vendor').val(paramProduct.productVendor);
        $('#input-product-quantity').val(paramProduct.quantityInStock);
        $('#input-product-price').val(paramProduct.buyPrice);
    }

    // reset input
    function resetForm() {
        $('#select-product-line').val(0);
        $('#input-product-code').val('');
        $('#input-product-name').val('');
        $('#input-product-description').val('');
        $('#input-product-scale').val('');
        $('#input-product-vendor').val('');
        $('#input-product-quantity').val('');
        $('#input-product-price').val('');
    }
});
