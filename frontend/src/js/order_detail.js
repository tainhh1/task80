$(document).ready(() => {
    // load order to input
    $.getJSON('http://localhost:8080/api/orders', loadOrderToInput);
    function loadOrderToInput(paramOrders) {
        let vSelectElement = $('#select-order').select2({ theme: 'bootstrap4' });
        paramOrders.forEach((order) => {
            $('<option>', {
                text: order.id,
                value: order.id,
            }).appendTo(vSelectElement);
        });
    }

    // load Product to input
    $.getJSON('http://localhost:8080/api/products', loadProductToInput);
    function loadProductToInput(paramProduct) {
        let vSelectElement = $('#select-product').select2({ theme: 'bootstrap4' });
        paramProduct.forEach((product) => {
            $('<option>', {
                text: product.productName,
                value: product.id,
            }).appendTo(vSelectElement);
        });
    }

    // khai bao table
    let gOrderDetailTable = $('#table-order').DataTable({
        columns: [
            { data: 'orderId' },
            { data: 'productName' },
            { data: 'quantityOrder' },
            { data: 'priceEach' },
            { data: 'Action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });

    function loadOrderToTable(paramOrder) {
        gOrderDetailTable.clear();
        gOrderDetailTable.rows.add(paramOrder);
        gOrderDetailTable.draw();
    }
    // add onchange cho select
    $('#select-order').change((e) => (gOrderId = e.target.value));
    $('#select-product').change((e) => (gProductId = e.target.value));
    // object order detail
    let gOrderDetailId = 0;
    let gProductId = 0;
    let gOrderId = 0;
    let gOrderDetail = {
        // get All Order Detail
        getAllOrderDetail() {
            $.getJSON('http://localhost:8080/api/order-details/product-name', loadOrderToTable);
        },
        // create & update order detail
        onCreateOrderDetailClick() {
            gOrderDetailId = 0;
            $('#select-order').prop('disabled', false);
            $('#select-product').prop('disabled', false);
            $('#modal-save').modal('show');
            resetInput();
        },
        onUpdateOrderDetailClick() {
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOrderDetailTable.row(vSelectedRow).data();
            $('#select-order').prop('disabled', true);
            $('#select-product').prop('disabled', true);
            $('#modal-save').modal('show');
            gOrderDetailId = vSelectedData.id;
            $.getJSON(
                `http://localhost:8080/api/order-details/${gOrderDetailId}`,
                loadOrderDetailToInput,
            );
        },
        // save Order Detail
        newOrderDetail: {
            quantityOrder: '',
            priceEach: '',
        },
        onSaveOrderDetailClick() {
            this.newOrderDetail = {
                quantityOrder: $('#input-order-quantity-order').val().trim(),
                priceEach: $('#input-price-each').val().trim(),
            };
            if (validateOrderDetail(this.newOrderDetail)) {
                gOrderDetail.saveOrderDetail(this.newOrderDetail);
            }
        },
        saveOrderDetail(paramOrderDetail) {
            if (gOrderDetailId == 0) {
                $.ajax({
                    type: 'post',
                    url: `http://localhost:8080/api/orders/${gOrderId}/order-details/products/${gProductId}`,
                    data: JSON.stringify(paramOrderDetail),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`successfully create new order detail`);
                        gOrderDetail.getAllOrderDetail();
                        $('#modal-save').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    type: 'put',
                    url: `http://localhost:8080/api/order-details/${gOrderDetailId}`,
                    data: JSON.stringify(paramOrderDetail),
                    contentType: 'application/json',
                    success: function (response) {
                        alert(`successfully update order detail`);
                        gOrderDetail.getAllOrderDetail();
                        $('#modal-save').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // delete order detail
        onDeleteOrderDetailClick() {
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gOrderDetailTable.row(vSelectedRow).data();
            gOrderDetailId = vSelectedData.id;
            $('#modal-delete').modal('show');
        },
        onDeleteAllOrderDetailClick() {
            gOrderDetailId = 0;
            $('#modal-delete').modal('show');
        },
        onConfirmDeleteOrderDetailClick() {
            if (gOrderDetailId == 0) {
                $.ajax({
                    type: 'delete',
                    url: 'http://localhost:8080/api/order-details',
                    success: function (response) {
                        alert(`successfully delete All oder detail`);
                        $('#modal-delete').modal('hide');
                        gOrderDetail.getAllOrderDetail();
                    },
                    error: (e) => alert(e),
                });
            } else {
                $.ajax({
                    type: 'delete',
                    url: `http://localhost:8080/api/order-details/${gOrderDetailId}`,
                    success: function (response) {
                        alert(`successfully delete All oder detail`);
                        $('#modal-delete').modal('hide');
                        gOrderDetail.getAllOrderDetail();
                    },
                    error: (e) => alert(e),
                });
            }
        },
    };
    gOrderDetail.getAllOrderDetail();

    // add event listener
    $('#btn-create-order-detail').click(gOrderDetail.onCreateOrderDetailClick);
    $('#table-order').on('click', '.fa-edit', gOrderDetail.onUpdateOrderDetailClick);
    $('#btn-save-info').click(gOrderDetail.onSaveOrderDetailClick);
    $('#table-order').on('click', '.fa-trash-alt', gOrderDetail.onDeleteOrderDetailClick);
    $('#btn-delete-all-order-detail').click(gOrderDetail.onDeleteAllOrderDetailClick);
    $('#btn-confirm-delete').click(gOrderDetail.onConfirmDeleteOrderDetailClick);

    // validate Order Detail
    function validateOrderDetail(paramOrderDetail) {
        let vResult = true;
        try {
            if (paramOrderDetail.quantityOrder == '') {
                vResult = false;
                throw `100. quantity can't be empty`;
            }
            if (paramOrderDetail.priceEach == '') {
                vResult = false;
                throw `200. Price can't be empty`;
            }
            if (gOrderDetailId == 0) {
                if (gOrderId == 0) {
                    vResult = false;
                    throw `300. Order can't be empty`;
                }
                if (gProductId == 0) {
                    vResult = false;
                    throw `400. Product can't be empty`;
                }
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // reset input
    function resetInput() {
        $('#select-order').val(0);
        $('#select-product').val(0);
        $('#input-order-quantity-order').val('');
        $('#input-price-each').val('');
    }

    // load order detail to input
    function loadOrderDetailToInput(paramOrderDetail) {
        $('#input-order-quantity-order').val(paramOrderDetail.quantityOrder);
        $('#input-price-each').val(paramOrderDetail.priceEach);
    }
});
