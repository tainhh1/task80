$(document).ready(() => {
    // get customer cho payment
    $.get(`http://localhost:8080/api/customers`, getAllCustomer);
    function getAllCustomer(paramCustomer) {
        let selectElement = $('#select-customer').select2({
            theme: 'bootstrap4',
        });
        paramCustomer.forEach((customer) => {
            $(`<option>`, {
                text: `${customer.firstName} ${customer.lastName}`,
                value: customer.id,
            }).appendTo(selectElement);
        });
    }

    // table payment
    let gPaymentTable = $('#table-payment').DataTable({
        columns: [
            { data: 'fullName' },
            { data: 'checkNumber' },
            { data: 'paymentDate' },
            { data: 'ammount' },
            { data: 'Action' },
        ],
        columnDefs: [
            {
                targets: -1,
                defaultContent: `<i class="far fa-edit text-primary"></i> | <i class="far fa-trash-alt text-danger"></i>`,
            },
        ],
    });
    // draw table
    function loadDataOnTable(paramPayments) {
        gPaymentTable.clear();
        gPaymentTable.rows.add(paramPayments);
        gPaymentTable.draw();
    }

    // add event change
    $('#select-customer').change((e) => (gCustomerId = e.target.value));

    let gPaymentId = 0;
    let gCustomerId = 0;
    let gPayment = {
        paymentDb: '',
        // check Number
        checkNumber(paramCheckNumber) {
            return this.paymentDb.some((payment) => payment.checkNumber == paramCheckNumber);
        },
        // get All Payment
        getAllPayment() {
            $.ajax({
                type: 'get',
                url: 'http://localhost:8080/api/fullname/payments',
                async: false,
                dataType: 'json',
                success: (payments) => {
                    gPayment.paymentDb = payments;
                    loadDataOnTable(payments);
                },
                error: (e) => alert(e.responseText),
            });
        },
        // create and update
        onCreatePaymentClick() {
            gPaymentId = 0;
            resetForm();
            $('#select-customer').prop('disabled', false);
            $('#modal-save').modal('show');
        },
        onUpdatePaymentClick() {
            $('#modal-save').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gPaymentTable.row(vSelectedRow).data();
            $('#select-customer').prop('disabled', true);
            gPaymentId = vSelectedData.id;
            $.get(`http://localhost:8080/api/payments/${gPaymentId}`, loadDataToInput, 'json');
        },
        // save payment
        newPayment: {
            checkNumber: '',
            paymentDate: '',
            amount: '',
        },
        onSavePaymentClick() {
            this.newPayment = {
                checkNumber: $('#input-payment-check-number').val().trim(),
                paymentDate: $('#input-payment-date').val().trim(),
                amount: parseFloat($('#input-payment-amount').val().trim()),
            };
            if (validatePayment(this.newPayment)) {
                gPayment.savePayment(this.newPayment);
            }
        },
        savePayment(paramPaymentData) {
            if (gPaymentId == 0) {
                $.ajax({
                    method: 'post',
                    url: `http://localhost:8080/api/customers/${gCustomerId}/payments`,
                    data: JSON.stringify(paramPaymentData),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: () => {
                        alert(`successfully create payment`);
                        gPayment.getAllPayment();
                        $('#modal-save').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    method: 'put',
                    url: `http://localhost:8080/api/payments/${gPaymentId}`,
                    data: JSON.stringify(paramPaymentData),
                    contentType: 'application/json',
                    dataType: 'json',
                    success: () => {
                        alert(`successfully update payment`);
                        gPayment.getAllPayment();
                        $('#modal-save').modal('hide');
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
        // delete payment
        onDeleteAllPaymentClick() {
            gPaymentId = 0;
            $('#modal-delete').modal('show');
        },
        onDeletePaymentClick() {
            $('#modal-delete').modal('show');
            let vSelectedRow = $(this).parents('tr');
            let vSelectedData = gPaymentTable.row(vSelectedRow).data();
            gPaymentId = vSelectedData.id;
        },
        onConfirmDeletePaymentClick() {
            if (gPaymentId == 0) {
                $.ajax({
                    method: 'delete',
                    url: `http://localhost:8080/api/payments`,
                    success: function (response) {
                        alert(`successfully deleted all payment`);
                        $('#modal-delete').modal('hide');
                        gPayment.getAllPayment();
                    },
                    error: (e) => alert(e.responseText),
                });
            } else {
                $.ajax({
                    method: 'delete',
                    url: `http://localhost:8080/api/payments/${gPaymentId}`,
                    success: function (response) {
                        alert(`successfully deleted payment`);
                        $('#modal-delete').modal('hide');
                        gPayment.getAllPayment();
                    },
                    error: (e) => alert(e.responseText),
                });
            }
        },
    };

    gPayment.getAllPayment();

    // add event listener
    $('#btn-create-payment').click(gPayment.onCreatePaymentClick);
    $('#table-payment').on('click', '.fa-edit', gPayment.onUpdatePaymentClick);
    $('#btn-save-info').click(gPayment.onSavePaymentClick);
    $('#table-payment').on('click', '.fa-trash-alt', gPayment.onDeletePaymentClick);
    $('#btn-delete-all-payment').click(gPayment.onDeleteAllPaymentClick);
    $('#btn-confirm-delete').click(gPayment.onConfirmDeletePaymentClick);

    // validate payment
    function validatePayment(paramPayment) {
        let vResult = true;
        try {
            if (paramPayment.checkNumber == '') {
                vResult = false;
                throw `100. check number can't be empty`;
            }
            if (paramPayment.paymentDate == '') {
                vResult = false;
                throw `200. Payment date can't be empty`;
            }
            if (paramPayment.amount == '') {
                vResult = false;
                throw `300. Amount can't be empty`;
            }
            if (gPaymentId == 0) {
                if (gCustomerId == 0) {
                    vResult = false;
                    throw 'Please select customer to create payment';
                }
            }
            if (gPaymentId == 0) {
                if (gPayment.checkNumber(paramPayment.checkNumber)) {
                    vResult = false;
                    throw `110. check number is existed`;
                }
            }
        } catch (error) {
            alert(error);
        }
        return vResult;
    }

    // load Payment To input
    function loadDataToInput(paramPayment) {
        $('#input-payment-check-number').val(paramPayment.checkNumber);
        $('#input-payment-date').val(paramPayment.paymentDate);
        $('#input-payment-amount').val(paramPayment.amount);
    }

    // reset form
    function resetForm() {
        $('#select-customer').val(0);
        $('#input-payment-check-number').val('');
        $('#input-payment-date').val('');
        $('#input-payment-amount').val('');
    }
});
